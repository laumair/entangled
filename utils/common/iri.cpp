#include <string_view>

#include "iri.hpp"

namespace iota {
namespace utils {
namespace iri {

std::shared_ptr<IRIMessage> payloadToMsg(std::string_view payload) {
  auto idx = payload.find(' ');
  auto what = payload.substr(0, idx);
  auto actual = payload.substr(idx + 1);

  if (what == "tx") {
    return std::make_shared<TXMessage>(actual);
  } else if (what == "sn") {
    return std::make_shared<SNMessage>(actual);
  }else if (what == "lmhs") {
    return std::make_shared<LMHSMessage>(actual);
  }

  return nullptr;
}
}  // namespace iri
}  // namespace utils
}  // namespace iota
